package com.dialog.zipkin.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;


@SpringBootApplication
@EnableZipkinStreamServer
public class DialogZipkinServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DialogZipkinServiceApplication.class, args);
	}
}
