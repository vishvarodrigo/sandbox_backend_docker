package com.dialog.zuul.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class DialogZuulServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DialogZuulServiceApplication.class, args);
	}
}
