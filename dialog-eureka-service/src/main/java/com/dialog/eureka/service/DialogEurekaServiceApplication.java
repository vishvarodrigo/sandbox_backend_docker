package com.dialog.eureka.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
@EnableEurekaClient
public class DialogEurekaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DialogEurekaServiceApplication.class, args);
	}
}
