package com.dialog.test.service.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dialog.test.service.services.TestService;

@RestController
public class TestController {
	
	private TestService testService;
	
	@Autowired
	public TestController(TestService testService) {
		this.testService = testService;
	}
	
	@RequestMapping(value="/google",method=RequestMethod.GET)
	public Map<String,Object> Test(){
		Map<String,Object> response = new HashMap<>();
		
		try {
			this.testService.TestMethod();
			
			response.put("STATUS", "SUCCESS");
			response.put("MESSAGE", "METHOD SUCCESSFULLY TRIGERRED");
		}catch(Exception ex) {
			response.put("STATUS", "FAILED");
			response.put("MESSAGE", "METHOD TRIGER FAILED : "+ex.getMessage());
		}
		
		return response;
	}
}
