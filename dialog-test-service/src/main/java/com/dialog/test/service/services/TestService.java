package com.dialog.test.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class TestService {

	private RestTemplate restTemplate;
	
	@Autowired
	public TestService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@HystrixCommand(fallbackMethod="defaultTestMethod")
	public void TestMethod() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", "application/json");
		
		HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
		
		restTemplate.exchange("http://www.google.lk", HttpMethod.GET, httpEntity, String.class);
		
	}
	
	// if something went wrong on above method, this will be executed
	public void fallbackTestMethod () {
		throw new RuntimeException();
	}
}
